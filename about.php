<div class="menu-spacer"></div>
<?php /* Template Name: About */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>

<?php $Banner = get_fields('56'); $about = get_fields('32');  ?>


<div class="breadcrumbs">
    <div class="content">
        <a href="/" class="list-item py-2">Home</a>
        <label class="py-2"> / </label>
        <label class="py-2">About us</label>
    </div>
</div>

<div class="content">
    <div class="homepage-about py-section" data-aos="fade-up">
        <?php $i=0; foreach ($about['about_us_paragraphs'] AS $one){ if($one['show_at_the_end']!=1){ $i++; ?>


        <div class="row align-items-center <?php if($i<sizeof($about['about_us_paragraphs'])){ ?> mb-4 <?php } ?>">
            <div class="col-md-6 <?php if($i%2==0){ ?> order-lg-1 <?php } ?>">
                <div class="image-max-width">
                    <div class="ratio-1-1 d-block cover" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>" style="background-image: url('<?php echo  $one['image']; ?>')"></div>
                </div>
            </div>
            <div class="col-md-6 mt-md-0 mt-3  <?php if($i%2==0){ ?> order-lg-0  pr-lg-big <?php }else{ ?> pl-lg-big <?php } ?>">
                <div class="title-group mb-3">
                    <label data-aos="fade-right" data-aos-delay="100"><?php echo $one['label']; ?></label>
                    <?php if(isset($one['title']) && $one['title']!=""){ ?>
                        <h2 data-aos="fade-right" data-aos-delay="200"><?php echo $one['title']; ?></h2>
                    <?php } ?>
                </div>
                <div class="subtitle text-gray"  data-aos="fade-right" data-aos-delay="<?php echo 300+$i*50; ?>"><?php echo nl2br($one['text']); ?></div>
            </div>
        </div>



        <?php } } ?>
    </div>




    <div class="homepage-about pb-section" data-aos="fade-up">

        <div class="row align-items-center">
            <div class="col-md-6 pr-lg-big">
                <div class="title-group">
                    <label data-aos="fade-right" data-aos-delay="100" class="title-tertiary"><?php echo $Banner['about_us_listing_label']; ?></label>
                    <h2 data-aos="fade-right" data-aos-delay="200"><?php echo $Banner['about_us_title']; ?></h2>
                </div>

                <div class="my-4">
                    <?php $i=0; foreach ($Banner['about_us_list'] AS $one){ $i++; ?>
                        <div class="list-item" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>" onclick="$(this).toggleClass('active')">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="title"><?php echo $one['title'] ?></div>
                                <div class="icon" style="background-image: url('/wp-content/themes/i4/assets/images/icon-plus.svg')"></div>
                            </div>

                            <div class="text"><?php echo $one['text'] ?></div>
                        </div>
                    <?php } ?>
                </div>

            </div>
            <div class="col-md-6 mt-md-0 mt-4">
                <div class="image-max-width">
                    <div class="ratio-5-4 cover" data-aos="fade-up" data-aos-delay="500" style="background-image: url('<?php echo  $Banner['about_us_image']; ?>')"></div>
                </div>
            </div>
        </div>

    </div>


    <div class="" id="team"></div>
    <div class="team">
<!--        <div class="title-group">-->
<!--            <label class="title-tertiary text-center"  data-aos="fade-up" data-aos-delay="100">--><?php //echo $about['team_label']; ?><!--</label>-->
<!--            <h2 class="title-primary text-center"  data-aos="fade-up" data-aos-delay="200">--><?php //echo $about['team_title']; ?><!--</h2>-->
<!--        </div>-->

<!--        <div class="row mt-4">-->
<!--            --><?php //$i=0; foreach ($about['team_list'] AS $one){ $i++; ?>
<!--                <div class="team-card col-lg-3 col-md-4 col-sm-6 mb-3" data-aos="fade-up" data-aos-delay="--><?php //echo 300+$i*50; ?><!--">-->
<!--                    <div class="cover ratio-1-1" style="background-image: url('<?php //echo $one['image']; ?>')"></div>-->
<!--                    <div class="d-flex flex-wrap align-items-center justify-content-between">-->
<!--                        <div>-->
<!--                            <h3 class="name mt-3 mb-2">--><?php //echo $one['full_name']; ?><!--</h3>-->
<!--                            <div class="position">--><?php //echo $one['job_position']; ?><!--</div>-->
<!--                        </div>-->
<!--                        <div class="d-flex align-items-center">-->
<!--                            --><?php //if(isset($one['email']) && $one['email']!=""){ ?><!--<a href="mailto:--><?php //echo $one['email']; ?><!--" class="icon"><i class="fas fa-envelope"></i></a>--><?php //} ?>
<!--                            --><?php //if(isset($one['linked_in_url']) && $one['linked_in_url']!=""){ ?><!--<a href="--><?php //echo $one['linked_in_url']; ?><!--" class="icon ml-2"><i class="fab fa-linkedin-in"></i></a>--><?php //} ?>
<!--                            <a href="mailto:--><?php //echo $one['email']; ?><!--" class="icon"><i class="fas fa-envelope"></i></a>-->
<!--                            <a href="--><?php //echo $one['linked_in_url']; ?><!--" class="icon ml-2"><i class="fab fa-linkedin-in"></i></a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            --><?php //} ?>
<!--        </div>-->


        <div class="pb-section">
            <div class="title-group">
                <label class="title-tertiary text-center"  data-aos="fade-up" data-aos-delay="100"><?php echo $Banner['fact_and_figures_label']; ?></label>
                <h2 class="title-primary text-center"  data-aos="fade-up" data-aos-delay="200"><?php echo $Banner['facts_and_figures_title']; ?></h2>
            </div>

            <div class="row mt-4">
                <?php $i=0; foreach ($Banner['facts_and_figures'] AS $one){ $i++;  ?>
                    <div class="col-lg-3 col-6 mt-lg-0 mb-3">
                        <div class="fact-and-figure-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                            <h3 class="label mb-2 mt-0">
                                <?php echo $one['label'] ?>
                            </h3>
                            <div class="line"></div>
                            <div class="text">
                                <?php echo $one['text'] ?>
                            </div>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>


        <div class="homepage-about pb-section" data-aos="fade-up">
            <?php $i=0; foreach ($about['about_us_paragraphs'] AS $one){ if($one['show_at_the_end']==1){ $i++; ?>


                <div class="row align-items-center <?php if($i<sizeof($about['about_us_paragraphs'])){ ?> mb-4 <?php } ?>">
                    <div class="col-md-6 <?php if($i%2==0){ ?> order-lg-1 <?php } ?>">
                        <div class="image-max-width">
                            <div class="ratio-1-1 d-block cover" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>" style="background-image: url('<?php echo  $one['image']; ?>')"></div>
                        </div>
                    </div>
                    <div class="col-md-6 mt-md-0 mt-3  <?php if($i%2==0){ ?> order-lg-0  pr-lg-big <?php }else{ ?> pl-lg-big <?php } ?>">
                        <div class="title-group mb-3">
                            <label data-aos="fade-right" data-aos-delay="100"><?php echo $one['label']; ?></label>
                            <?php if(isset($one['title']) && $one['title']!=""){ ?>
                                <h2 data-aos="fade-right" data-aos-delay="200"><?php echo $one['title']; ?></h2>
                            <?php } ?>
                        </div>
                        <div class="subtitle text-gray"  data-aos="fade-right" data-aos-delay="<?php echo 300+$i*50; ?>"><?php echo nl2br($one['text']); ?></div>
                    </div>
                </div>

            <?php } } ?>
        </div>


    </div>





</div>





<!---->
<!---->
<?php // $pages = get_pages([ "child_of" => get_the_ID(), "parent" => get_the_ID(), "sort_column" => "menu_order" ]); ?>
<!---->
<?php
//foreach ($pages AS $page){
//    $html = file_get_contents($page->guid); echo $html;
//} ?>







<?php /* ?>
<div class="homepage-testimonials parallax-window" data-parallax="scroll" data-image-src="<?php echo $Banner['testimonials_background_image']; ?>" data-aos="fade-in">
    <div class="content">
        <div class="title-group text-center content-smaller" data-aos="fade-up">
            <label data-aos="fade-up" data-aos-delay="100"><?php echo $Banner['testimonials_label']; ?></label>
            <h2 class="text-white" data-aos="fade-up" data-aos-delay="200"><?php echo $Banner['testimonials_tittle']; ?></h2>
        </div>
        <div class="subtitle text-white"  data-aos="fade-up" data-aos-delay="300"><?php echo $Banner['testimonials_description']; ?></div>
        <sep></sep><sep></sep><sep></sep><sep></sep>
        <div class="carousel cards-carousel-full dark" style="min-height: 300px;"  data-aos="fade-up" data-aos-delay="400">
            <?php foreach (get_posts([ "category_name" => "Testimonial"]) AS $testimonial){ $PostMeta = get_post_meta($testimonial->ID);  ?>
                <section class="carousel-item" href="javascript:;">
                    <div class="image" style="background-image: url('<?php echo wp_get_attachment_image_src($PostMeta['image'][0])[0]; ?>')"></div>
                    <div class="right">
                        <h3><?php echo $PostMeta['full_name'][0]; ?></h3>
                        <div class=""><?php echo $PostMeta['position'][0]; ?></div>
                    </div>
                    <div class="text"><?php echo $PostMeta['testimonial'][0]; ?></div>
                </section>
            <?php } ?>
        </div>
    </div>
</div>
<?php */ ?>





<?php get_footer(); ?>


