$(document).ready(function(){
    $('[data-youtube]').youtube_background();

    window.addEventListener('scroll', InitializeMenuScroll);
    function InitializeMenuScroll(){
        var scroll_start = 0;
        if (($(document).scrollTop() - 40) > 0){
            $('#main_menu').addClass('compact');
            $('#main_menu .custom-logo').attr('src','/wp-content/themes/i4/assets/images/logo.svg');
            $('header.main_menu nav a.toggle i').css('color','#000');
        }else{
            $('#main_menu').removeClass('compact');
            $('#main_menu .custom-logo').attr('src','/wp-content/uploads/2021/02/i4-logo.png');
            $('header.main_menu nav a.toggle i').css('color','#fff');
        }
    }




    if($(".multi-carousel").length>0) {

        var owl2 = $(".multi-carousel")
        owl2.owlCarousel({
            //items:,
            autoWidth:true,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplaySpeed: 2000,
            autoplayHoverPause: true,
            // rtl: false,
            loop: false,
            nav: false,
            dots:false,
            margin: 30,
            navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>']
            // animateOut: 'slideOutUp',
            // animateIn: 'slideInUp'
        });
        owl2.on('changed.owl.carousel', function(event) {
            bLazy.revalidate();
        });
        owl2.on('initialized.owl.carousel', function(event) {
            bLazy.revalidate();
        });
    }



    var rellax = new Rellax('.rellax', {
        speed: -2,
        center: false,
        wrapper: null,
        round: true,
        vertical: true,
        horizontal: false
    });


    $('.cards-carousel').carousel({
        dist: 0,
        indicators: true,
        padding:60,
        noWrap:true,
    });
    $('.cards-carousel-blue').carousel({
        dist: 0,
        indicators: true,
        padding:60,
        noWrap:true,
        // fullWidth: true
    });



    $('.cards-carousel-full').carousel({
        dist: 0,
        indicators: true,
        padding:60,
        noWrap:true,
        fullWidth: true
    });



    $('.accordion a').click(function(){
        $(this).parent().toggleClass('expanded');
    });

    $('.modal').modal({
        onCloseStart : function(elem){ $(elem).find('iframe').attr( 'src', function ( i, val ) { return val; }); }
    });
    AOS.init({ easing: 'ease-in-out-sine', duration: 1000, once : true });

    $('ul.mobile .menu-item-has-children a').click(function(){
        $(this).parent().find('ul.sub-menu').first().toggleClass('expanded');
    });
    $('.main_menu .toggle').click(function(){
        $(this).toggleClass('expanded');
        $('ul.mobile').toggleClass('expanded');
    })
});



function carouselPrev(elem){
    // $(elem).hide();
    var elems = $(elem).parents('.carousel-container').find('.carousel')
    var moveRight = M.Carousel.getInstance(elems);
    moveRight.prev(1);
}

function carouselNext(elem){
    // $(elem).hide();
    var elems = $(elem).parents('.carousel-container').find('.carousel')
    var moveRight = M.Carousel.getInstance(elems);
    moveRight.next(1);
}
