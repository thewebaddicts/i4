<?php /* Template Name: Careers */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>
<?php $page = get_fields('36'); ?>


<div class="breadcrumbs">
    <div class="content">
        <a href="/" class="list-item py-2">Home</a>
        <label class="py-2"> / </label>
        <label class="py-2">Careers</label>
    </div>
</div>

<div class="content pb-section mt-5">
    <div class="contact-us-form">
        <div class="title-group">
            <label class="text-center" data-aos="fade-up" data-aos-delay="100"><?php echo $page['careers_label'] ?></label>
            <h2 class="text-center" data-aos="fade-up" data-aos-delay="200"><?php echo $page['careers_title'] ?></h2>
            <div class="text-center mt-3" data-aos="fade-up" data-aos-delay="300" style="font-weight:600"><?php echo nl2br($page['careers_description']); ?></div>
        </div>
        <div class="careers-form" data-aos="fade-up" data-aos-delay="400">
            <form  action="" method="POST" enctype="multipart/form-data">
            <div class="row mt-4">
                <div class="col-md-6">
                    <h5 class="label"><?php echo $page['first_name_label'] ?></h5>
                    <input type="text" name="first_name" placeholder="<?php echo $page['first_name_label'] ?>" required>
                </div>
                <div class="col-md-6">
                    <h5 class="label"><?php echo $page['last_name_label'] ?></h5>
                    <input type="text" name="last_name" placeholder="<?php echo $page['last_name_label'] ?>" required>
                </div>
                <div class="col-md-6">
                    <h5 class="label"><?php echo $page['email_label'] ?></h5>
                    <input type="email" name="email" placeholder="<?php echo $page['email_label'] ?>" required>
                </div>
                <div class="col-md-6">
                    <h5 class="label"><?php echo $page['upload_cv_label'] ?></h5>
                    <div class="upload-btn-wrapper">
                        <button class="upload-btn">Upload &nbsp; <i class="fas fa-upload"></i></button>
                        <input type="file" name="attachment" required />
                    </div>
                </div>
                <div class="col-12">
                    <h5 class="label"><?php echo $page['cover_label'] ?></h5>
                    <textarea name="cover_letter" placeholder="<?php echo $page['cover_label'] ?>" required></textarea>
                </div>
                <div class="col-12 mt-3">
                    <div class="d-flex justify-content-center">
                        <button type="submit" name="submit" class="btn-submit"><?php echo $page['careers_button_label'] ?></button>
                    </div>
                </div>
            </div>
                <?php

                //                $my_custom_filename = time() . $_FILES['attachment']['name'];
                //                wp_upload_bits($my_custom_filename , null, file_get_contents($_FILES['fileToUpload']['tmp_name']));

                $postData = $uploadedFile = $statusMsg = '';
                $msgClass = 'errordiv';
                if(isset($_POST['submit'])){
                    // Get the submitted form data
                    $postData = $_POST;
                    $email = $_POST['email'];
                    $name = $_POST['first_name'].' '.$_POST['last_name'];
                    $subject = 'New Career From Submit';
                    $message = $_POST['cover_letter'];

                    // Check whether submitted data is not empty
                    if(!empty($email) && !empty($name) && !empty($subject) && !empty($message)){

                        // Validate email
                        if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
                            echo 'Please enter your valid email.';
                        }else{
                            $uploadStatus = 1;

                            // Upload attachment file
                            if(!empty($_FILES["attachment"]["name"])){

                                // File path config
                                $targetDir = "wp-content/uploads/files/";
                                $fileName = time() . $_FILES['attachment']['name'];
                                $targetFilePath = $targetDir . $fileName;
                                $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

                                // Allow certain file formats
                                $allowTypes = array('pdf', 'doc', 'docx');
                                if(in_array($fileType, $allowTypes)){
                                    // Upload file to the server
                                    if(move_uploaded_file($_FILES["attachment"]["tmp_name"], $targetFilePath)){
                                        $uploadedFile = $targetFilePath;
                                    }else{
                                        $uploadStatus = 0;
                                        echo "Sorry, there was an error uploading your file.";
                                    }
                                }else{
                                    $uploadStatus = 0;
                                    echo 'Sorry, only PDF and DOC files are allowed to upload.';
                                }
                            }

                            if($uploadStatus == 1){

                                // Recipient
                                $toEmail = trim($page['send_form_to_email']);

                                // Subject
                                $emailSubject = 'Career form new submit';

                                // Message
                                $htmlContent = '<h2>Career Form Submitted</h2>
                    <p><b style="color:#00A48E">Full Name:</b> '.$name.'</p>
                    <p><b style="color:#00A48E">Email:</b> '.$email.'</p>
                    <p><b style="color:#00A48E">Message:</b><br/>'.$message.'</p>';

                                // Header for sender info

                                if(!empty($uploadedFile) && file_exists($uploadedFile)){


                                    // Multipart boundary
                                    $message = "\n\n" . $htmlContent . "\n\n";

                                    // Preparing attachment
                                    if(is_file($uploadedFile)){
                                        $fp =    @fopen($uploadedFile,"rb");
                                        $data =  @fread($fp,filesize($uploadedFile));
                                        @fclose($fp);
                                        $data = chunk_split(base64_encode($data));
                                        $message .= '<b style="color:#00A48E">CV file:</b> <a href="'.get_site_url().'/'.$targetFilePath.'">'.$fileName.'</a>';
                                    }




                                    // Send email
                                    sendmail($emailSubject, $toEmail, $message);

                                    // Delete attachment file from the server
//                    @unlink($uploadedFile);
                                }else{
                                    // Set content-type header for sending HTML email
                                    $headers .= "\r\n". "MIME-Version: 1.0";
                                    $headers .= "\r\n". "Content-type:text/html;charset=UTF-8";

                                    // Send email
                                    sendmail($emailSubject, $toEmail, $htmlContent);
                                }

                            }
                        }
                    }else{
                        $statusMsg = 'Please fill all the fields.';
                    }
                }




                ?>

            </form>
        </div>

    </div>
</div>






<?php get_footer(); ?>


