<?php /* Template Name: Competencies  */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>
<?php $page = get_fields('2'); ?>

<?php $Banner = get_fields('24'); ?>

<div class="breadcrumbs">
    <div class="content">
        <a href="/" class="list-item py-2">Home</a>
        <label class="py-2"> / </label>
        <label class="py-2">Competencies</label>
    </div>
</div>

<div class="content">
    <div class="py-section">
        <div class="title-group">
            <label class="title-tertiary text-center" data-aos="fade-up" data-aos-delay="100"><?php echo $page['sectors_label']; ?></label>
            <h2 class="title-primary text-center" data-aos="fade-up" data-aos-delay="200"><?php echo $page['sectors_title']; ?></h2>
        </div>

        <div class="row mt-4">
            <?php $i=0; foreach ($page['sectors_list'] AS $one){ $i++;  ?>
                <div class="col-lg-2 col-md-3 col-6 mb-3">
                    <div class="sector-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                        <div class="icon"><?php echo $one['icon'] ?></div>
                        <h4 class="title"><?php echo $one['title'] ?></h4>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>


    <div class="">
        <div class="row align-items-center">
            <div class="col-lg-6 pr-lg-big">
                <div class="title-group">
                    <label class="title-tertiary" data-aos="fade-up" data-aos-delay="100"><?php echo $page['competencies_label']; ?></label>
                    <h2 class="title-primary" data-aos="fade-up" data-aos-delay="200"><?php echo $page['competencies_title']; ?></h2>
                    <div class="text mt-4 t-16px" data-aos="fade-up" data-aos-delay="300"><?php echo nl2br($page['competencies_text']); ?></div>
                </div>
            </div>
            <div class="col-lg-6 mt-lg-0 mt-3">
                <div class="image-max-width" data-aos="fade-up" data-aos-delay="200">
                    <div class="contain ratio-5-4" style="background-image: url('<?php echo $page['competencies_image']; ?>')"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="py-section">
        <?php $i=0; foreach ($page['competencies_list'] AS $one){ $i++; ?>
            <div class="" id="<?php echo $one['title']; ?>">
            <div class="row competencies align-items-center <?php if($i<sizeof($page['competencies_list'])){ ?> mb-lg-5 mb-4 <?php } ?>" >
                <div class="col-lg-6  <?php if($i%2==0){ ?> order-lg-1 pl-lg-big <?php }else{ ?> pr-lg-big <?php } ?>">
                    <h3 class="title" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>"><?php echo $one['title']; ?></h3>
                    <?php $j=0; foreach ($one['list'] AS $item){ $j++;  ?>
                        <div class="list-item" id="competency_list_item" data-aos="fade-up" data-aos-delay="<?php echo 300+$j*50; ?>">
                            <div class="d-flex align-items-center">
                                <div class="list-icon contain" style="background-image: url('/wp-content/themes/i4/assets/images/icon-right.svg')"></div>
                                <div class="list-title pl-2"><?php echo $item['title'] ?></div>
                            </div>
    <!--                        <div class="text">--><?php //echo $item['text'] ?><!--</div>-->
                        </div>
                    <?php } ?>
                </div>
                <div class="col-lg-6  <?php if($i%2==0){ ?> order-lg-0  <?php } ?> ">
                    <div class="image-max-width" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                        <div class="cover ratio-5-4" style="background-image: url('<?php echo $one['image']; ?>')"></div>
                    </div>
                </div>
            </div>
            </div>
        <?php } ?>

    </div>






</div>




<?php get_footer(); ?>

