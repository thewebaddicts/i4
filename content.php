<?php /* Template Name: Main Template for Solutions & Services */ ?>
<?php get_header(); ?>
<?php $page = get_fields(get_the_ID()); ?>
<?php global $post;  ?>


<div class="menu-spacer"></div>


<div class="breadcrumbs">
    <div class="content">
        <a href="/" class="list-item py-2">Home</a>
        <label class="py-2"> / </label>
        <label class="py-2"><?php echo get_the_title($post->post_parent); ?> </label>
        <label class="py-2"> / </label>
        <label class="py-2"><?php echo $page['solution_title'];  ?> </label>
    </div>
</div>






    <div class="content py-section">
        <div class="row align-items-center pb-section">
            <div class="col-lg-6">
                <h1 data-aos="fade-right" data-aos-delay="100" class="title-primary my-0"><?php echo $page['solution_title']; ?></h1>
                <div class="subtitle" data-aos="fade-right" data-aos-delay="200"><?php echo $page['solution_text']; ?></div>
                <a class="d-block mt-3" data-aos="fade-right" data-aos-delay="300" style="font-size:14px" href="<?php echo get_page_link(39); ?>">Contact Us to Learn More</a>
            </div>
            <div class="col-lg-6 mt-lg-0 mt-3">
                <div class="image-max-width" data-aos="fade-up" data-aos-delay="<?php echo 150; ?>">
                    <div class="cover ratio-5-4" style="background-image: url('<?php echo $page['solution_image']; ?>')"></div>
                </div>
            </div>
        </div>

        <div class="">
            <div class="row">
            <?php $i=0; foreach($page['solution_points'] as $point) { ?>
                <div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="<?php echo $i; ?>">
                    <div class="solution-card">
                        <div class="label"><?php echo $point['title']; ?></div>
                        <div class="text"><?php echo $point['text']; ?></div>
                    </div>
                </div>
            <?php $i=$i+50; } ?>
            </div>
        </div>

    </div>




<?php  $pages = get_pages([ "child_of" => get_the_ID(), "parent" => get_the_ID(), "sort_column" => "menu_order" ]);  ?>

<?php
foreach ($pages AS $page){
    $html = file_get_contents($page->guid);  echo $html;
} ?>


<?php /*

<?php if(have_posts()) : ?>
    <?php while(have_posts())  : the_post(); ?>
        <h2><?php the_title(); ?></h2>
        <?php the_content(); ?>
        <?php comments_template( '', true ); ?>
    <?php endwhile; ?>
<?php else : ?>
    <h3><?php _e('404 Error&#58; Not Found'); ?></h3>
<?php endif; ?>


*/ ?>

<?php get_footer(); ?>

