
<?php $info = get_fields('56'); ?>
<div class="footer">
    <div id="map" style="min-height:500px;" data-aos="fade-in"></div>


    <!--<pre>--><?php //print_r($meta); ?><!--</pre>-->

    <script language="javascript">
        // eslint-disable no-undef
        let map;

        // Initialize and add the map
        function initMap() {
            const myLatLng = { lat: <?php echo $info['latitude']; ?>, lng: <?php echo $info['longitude']; ?> };
            map = new google.maps.Map(document.getElementById("map"), {
                center: myLatLng,
                zoom: 17,
                styles: [
                    {
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#bdbdbd"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dadada"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#c9c9c9"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    }
                ]
            });



            new google.maps.Marker({
                position: myLatLng,
                map,
                title: "Hello World!",
                icon: "<?php echo get_template_directory_uri(); ?>/assets/images/pin.png"
            });


        }
    </script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdT99wKL5bEyvYOYdI4nHvuUKMQICWZfY&callback=initMap&libraries=&v=beta&map_ids=3a3b33f0edd6ed2a"
            defer
    ></script>


    <div class="footer-menu">


        <div class="footer-center">
            <h3 class="title text-center my-2"><?php echo $info['footer_title']; ?></h3>
            <div class="description text-center"><?php echo $info['footer_description']; ?></div>
            <div class="d-md-flex align-items-center mt-3">
                <div class="text-center width-50 border-right pr-md-5 mr-md-5">
                    <div class="icon contain" style="background-image: url('/wp-content/themes/i4/assets/images/icon-phone-footer.svg')"></div>
                    <h4 class="div-title">By phone</h4>
                    <a class="btn-outline mt-5" href="tel:<?php echo $info['footer_phone'] ?>" target="_blank"><?php echo $info['footer_phone'] ?></a>
                </div>
                <div class="text-center width-50 mt-md-0 mt-4">
                    <div class="icon contain" style="background-image: url('/wp-content/themes/i4/assets/images/icon-email-footer.svg')"></div>
                    <h4 class="div-title">By email</h4>
                    <a href="mailto:<?php echo $info['footer_email']; ?>" class="btn-outline mt-5"><?php echo $info['footer_email']; ?></a>
                </div>
            </div>
        </div>


        <div class="content">
            <div class="row">
                <div class="col-lg-3 col-md-4 mt-md-0 mt-3"><h3>Sitemap</h3><?php wp_nav_menu([
                        "menu" => "footer",
                        "theme_location" => "primary",
                        "container" => "",
                        "items_wrao" => "<ul></ul>",
                        "depth" => 1,
                    ]); ?></div>


                <div class="col-lg-3 col-md-4 mt-md-0 mt-3">
                    <h3>Solutions & Services</h3>
<!--                    --><?php //$solutions = get_fields('2');  ?>
<!--                    --><?php //foreach ($solutions['competencies_list'] as $solution){ ?>
<!--                        <a href="--><?php //echo get_page_link('2')."#".$solution['title']; ?><!--" class="footer-link">--><?php //echo $solution['title']; ?><!--</a>-->
<!---->
<!--                    --><?php //} ?>
                    <?php  $pages = wp_list_pages( array('child_of' => '316', 'title_li' => '') );?>
                    <?php $services= wp_list_pages( array('child_of' => '328', 'title_li' => '') ); ?>


                </div>

                <div class="col-lg-3 col-md-4 mt-md-0 mt-3">
                    <h3>Location</h3>
                    <div class="footer-item"><?php echo nl2br($info['location_details']); ?></div>
                </div>


    <!--            <div class="col-lg-3 hide-on-med-and-down">-->
    <!--                <h3>Location</h3>-->
    <!--                --><?php //$TopInfo = get_fields('56'); ?>
    <!--                <div class="footer-info">-->
    <!--                    <div><b>A</b>--><?php //echo $TopInfo['address'] ?><!--</div>-->
<!--                        <a href="tel:--><?php //echo $TopInfo['telephone_number'] ?><!--" target="_blank"><b>T</b>--><?php //echo $TopInfo['telephone_number'] ?><!--</a>-->
    <!--                    <a href="mailto:--><?php //echo $TopInfo['email'] ?><!--" target="_blank"><b>E</b>--><?php //echo $TopInfo['email'] ?><!--</a>-->
    <!--                </div>-->
    <!--            </div>-->


            </div>
        </div>

    </div>



    <div class="footer-bottom">
        <div class="content">
            <div class="d-lg-flex align-items-center justify-content-between">
                <div class="text-white t-opacity-50"><?php echo $info['copyrights']; ?></div>
                <div class="social-media-footer">
                    <?php if(isset($info['linkedin']) && is_array($info['linkedin'])){  if(array_key_exists("linkedin",$info) && array_key_exists("url",$info['linkedin']) && $info['linkedin']!=''){ ?><a href="<?php echo $info['linkedin']['url'] ?>" target="_blank" class="social"><?php echo htmlspecialchars_decode($info['linkedin']['title']); ?></a><?php }}  ?>
                    <?php if(isset($info['facebook']) && is_array($info['facebook'])){  if(array_key_exists("facebook",$info) && array_key_exists("url",$info['facebook']) && $info['facebook']['url']!=''){ ?><a href="<?php echo $info['facebook']['url'] ?>" target="_blank" class="social"><?php echo htmlspecialchars_decode($info['facebook']['title']); ?></a><?php } } ?>
                    <?php if(isset($info['twitter']) && is_array($info['twitter'])){  if(array_key_exists("twitter",$info) && array_key_exists("url",$info['twitter']) && $info['twitter']!=''){ ?><a href="<?php echo $info['twitter']['url'] ?>" target="_blank" class="social"><?php echo htmlspecialchars_decode($info['twitter']['title']); ?></a><?php }} ?>
                    <?php if(isset($info['youtube']) && is_array($info['youtube'])){  if(array_key_exists("youtube",$info) && array_key_exists("url",$info['youtube']) && $info['youtube']!=''){ ?><a href="<?php echo $info['youtube']['url'] ?>" target="_blank" class="social"><?php echo htmlspecialchars_decode($info['youtube']['title']); ?></a><?php }} ?>
                    <?php if(isset($info['google_plus']) && is_array($info['google_plus'])){  if(array_key_exists("google_plus",$info) && array_key_exists("url",$info['google_plus']) && $info['google_plus']!=''){ ?><a href="<?php echo $info['google_plus']['url'] ?>" target="_blank" class="social"><?php echo htmlspecialchars_decode($info['google_plus']['title']); ?></a><?php }} ?>

                </div>
            </div>


            <div class="mt-4">
                <div class="text-white t-opacity-50">Designed & Developed by The Web Addicts</div>
            </div>
        </div>
    </div>

</div>
<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/youtube-bg/jquery.youtube-background.js'; ?><!--"></script>-->
<!--<script language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.js"></script>-->
<?php
wp_footer();
?>
<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/aos/aos.js'; ?><!--"></script>-->
<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/parallax-js/parallax.min.js'; ?><!--"></script>-->
<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/init.js'; ?><!--"></script>-->
</body>
</html>
