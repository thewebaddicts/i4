<?php get_header(); ?>
<?php $Banner = get_fields('56'); ?>
<!--<pre>--><?php //print_r($Banner); ?><!--</pre>-->
<?php $competencies_page = get_fields('2'); ?>
<?php
$solutions = get_pages( array('child_of'    => '316', 'sort_column'=> 'menu_order'));
$services= get_pages( array('child_of' => '328', 'sort_column'=> 'menu_order') );

?>

<?php $rand=rand(0,count($Banner['youtube_video_url'])-1); ?>
<div class="menu-spacer"></div>
<div class="homepage-banner" data-aos="fade-in">
    <div id="ytplayer" data-youtube="<?php echo $Banner['youtube_video_url'][$rand]['url']; ?>"></div>
    <div class="content">
        <h1 data-aos="fade-right" data-aos-delay="100"><?php echo $Banner['youtube_video_url'][$rand]['title']; ?></h1>
        <div class="subtitle" data-aos="fade-right" data-aos-delay="200"><?php echo $Banner['youtube_video_url'][$rand]['subtitle']; ?></div>
        <a href="<?php echo $Banner['youtube_video_url'][$rand]['button_url']; ?>" class="btn-fill" data-aos="fade-right" data-aos-delay="300">
            <div class="label"> <?php echo $Banner['youtube_video_url'][$rand]['button_label']; ?></div>
        </a>
    </div>
</div>



<div class="homepage-about py-section" data-aos="fade-up">
    <div class="content">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="image-max-width">
                    <div class="ratio-1-1 d-block cover" data-aos="fade-up" data-aos-delay="500" style="background-image: url('<?php echo  $Banner['image']; ?>')"></div>
                </div>
            </div>
            <div class="col-md-6 pl-lg-big mt-md-0 mt-3">
                <div class="title-group mb-3">
                    <label data-aos="fade-right" data-aos-delay="100"><?php echo $Banner['about_us_label']; ?></label>
                    <h2 data-aos="fade-right" data-aos-delay="200"><?php echo $Banner['title']; ?></h2>
                </div>
                <div class="subtitle text-gray"  data-aos="fade-right" data-aos-delay="300"><?php echo nl2br($Banner['text']); ?></div>
                <a href="<?php echo $Banner['button_link']; ?>" class="btn-paragraph mt-3"  data-aos="fade-right" data-aos-delay="400"><?php echo $Banner['button_label']; ?></a>
            </div>
        </div>
    </div>
</div>


<div class="homepage-partners pb-section" data-aos="fade-up">
    <div class="content">
        <div class="title-group mb-lg-4 mb-3">
            <h2 data-aos="fade-right" data-aos-delay="200" class="text-center"><?php echo $Banner['partners_title']; ?></h2>
        </div>
        <div class="row">
            <?php $i=0; foreach ($Banner['partners_images'] AS $partner){ $i++; ?>

                <div class="col-lg-2 col-md-3 col-4 mt-1">
                    <div class="partner-image ratio-3-2" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>" style="background-image: url('<?php echo  $partner['partner_image']; ?>')"></div>
                </div>

            <?php } ?>

        </div>
    </div>
</div>

<!--<div id="about-youtube-modal" class="modal"><div class="video-container">--><?php //echo wp_oembed_get($Banner['about_video_url']['url'], [ "width"=> "580"]); ?><!--</div></div>-->

<!---->
<!--<div class="homepage-about py-section" data-aos="fade-up">-->
<!--    <div class="content">-->
<!--        <div class="row align-items-center">-->
<!--            <div class="col-md-6 pr-lg-big">-->
<!--                <div class="title-group">-->
<!--                    <label data-aos="fade-right" data-aos-delay="100" class="title-tertiary">--><?php //echo $Banner['about_us_listing_label']; ?><!--</label>-->
<!--                    <h2 data-aos="fade-right" data-aos-delay="200">--><?php //echo $Banner['about_us_title']; ?><!--</h2>-->
<!--                </div>-->
<!---->
<!--                <div class="my-4">-->
<!--                    --><?php //$i=0; foreach ($Banner['about_us_list'] AS $one){ $i++; ?>
<!--                        <div class="list-item" data-aos="fade-up" data-aos-delay="--><?php //echo 300+$i*50; ?><!--" onclick="$(this).toggleClass('active')">-->
<!--                            <div class="d-flex justify-content-between align-items-center">-->
<!--                                <div class="title">--><?php //echo $one['title'] ?><!--</div>-->
<!--                                <div class="icon" style="background-image: url('/wp-content/themes/i4/assets/images/icon-plus.svg')"></div>-->
<!--                            </div>-->
<!---->
<!--                            <div class="text">--><?php //echo $one['text'] ?><!--</div>-->
<!--                        </div>-->
<!--                    --><?php //} ?>
<!--                </div>-->
<!---->
<!--                <a href="--><?php //echo $Banner['about_us_button_link']."#team"; ?><!--" class="btn-paragraph"  data-aos="fade-right" data-aos-delay="400">--><?php //echo $Banner['about_us_button_label']; ?><!--</a>-->
<!--            </div>-->
<!--            <div class="col-md-6 mt-md-0 mt-4">-->
<!--                <div class="image-max-width">-->
<!--                    <div class="ratio-5-4 cover" data-aos="fade-up" data-aos-delay="500" style="background-image: url('<?php //echo  $Banner['about_us_image']; ?>')"></div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->



<div class="content facts-and-figures pb-section">
    <div class="title-group">
        <label class="title-tertiary text-center"><?php echo $Banner['fact_and_figures_label']; ?></label>
        <h2 class="title-primary text-center"><?php echo $Banner['facts_and_figures_title']; ?></h2>
    </div>

    <div class="row mt-4">
    <?php $i=0; foreach ($Banner['facts_and_figures'] AS $one){ $i++;  ?>
        <?php if(array_key_exists('display_homepage',$one) && $one['display_homepage']==1){ ?>
        <div class="col-lg-3 col-6 mt-lg-0 mt-3">
            <div class="fact-and-figure-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                <h3 class="label mb-2 mt-0">
                    <?php echo $one['label'] ?>
                </h3>
                <div class="line"></div>
                <div class="text">
                    <?php echo $one['text'] ?>
                </div>
            </div>
        </div>
        <?php } ?>
    <?php } ?>
    </div>
</div>

<!--Competencies-->
<!--<div class="py-section">-->
<!--    <div class="content">-->
<!--        <div class="d-lg-flex justify-content-between align-items-end">-->
<!--            <div class="title-group width-50 mb-lg-0 mb-3">-->
<!--                <label class="title-tertiary">--><?php //echo $Banner['competencies_label']; ?><!--</label>-->
<!--                <h2 class="title-primary ">--><?php //echo $Banner['competencies_title']; ?><!--</h2>-->
<!--            </div>-->
<!--            <a href="--><?php //echo $Banner['competencies_button']['link']; ?><!--" class="btn-paragraph">--><?php //echo $Banner['competencies_button']['label']; ?><!--</a>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="padding-left-custom">-->
<!--        <div class="multi-carousel owl-carousel mt-4">-->
<!--            --><?php //$i=0; foreach ($competencies_page['competencies_list'] AS $competency){ $i++;  ?>
<!--                <a href="--><?php //echo $Banner['competencies_button']['link']."#".$competency['title']; ?><!--" class="competency-card" data-aos="fade-up" data-aos-delay="--><?php //echo 300+$i*50; ?><!--" style="background-image: url('--><?php //echo  $competency['image']; ?><!--')">-->
<!--                    <div class="image cover ratio-5-4" style="background-image: url('--><?php //echo  $competency['image']; ?><!--')"></div>-->
<!--                    <h3 class="title">--><?php //echo $competency['title'] ?><!--</h3>-->
<!--                </a>-->
<!--            --><?php //} ?>
<!--        </div>-->
<!--    </div>-->
<!--</div>-->



<!--Solutions and Services-->
<div class="py-section">
    <div class="content">
        <div class="d-lg-flex justify-content-between align-items-end">
            <div class="title-group width-50 mb-lg-0 mb-3">
                <label class="title-tertiary"><?php echo $Banner['solutions_and_services_label']; ?></label>
                <h2 class="title-primary "><?php echo $Banner['solutions_and_services_title']; ?></h2>
            </div>
        </div>
    </div>
    <div class="padding-left-custom">
        <div class="multi-carousel owl-carousel mt-4">
            <?php $i=0; foreach ($solutions AS $solution){ $info = get_fields($solution->ID); $i++;  ?>
                <a href="<?php echo get_permalink($solution->ID); ?>" class="competency-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                    <div class="image cover ratio-5-4" style="background-image: url('<?php echo  $info['solution_image']; ?>')"></div>
                    <h3 class="title"><?php echo $info['solution_title'] ?></h3>
                </a>
            <?php } ?>
            <?php foreach ($services AS $service){ $ser = get_fields($service->ID); $i++;  ?>
                <a href="<?php echo get_permalink($service->ID); ?>" class="competency-card" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>">
                    <div class="image cover ratio-5-4" style="background-image: url('<?php echo  $ser['solution_image']; ?>')"></div>
                    <h3 class="title"><?php echo $ser['solution_title'] ?></h3>
                </a>
            <?php } ?>
        </div>
    </div>
</div>




<div class="content" data-aos="fade-up" data-aos-delay="100">
    <div class="careers-banner d-lg-flex justify-content-between align-items-center">
        <div class="width-50">
            <label class="title-tertiary"><?php echo $Banner['careers_label']; ?></label>
            <h1 class="title my-3"><?php echo $Banner['careers_title']; ?></h1>
            <div class="text"><?php echo $Banner['careers_text']; ?></div>
        </div>
        <a target="_blank" href="<?php echo $Banner['careers_button']['link'] ?>" class="btn-outline mt-lg-0 mt-3">
            <div class="label"><?php echo $Banner['careers_button']['label'] ?></div>
        </a>
    </div>
</div>


<div class="content py-section">
    <div class="row align-items-center">
        <div class="col-md-6 pr-lg-5 pr-md-4">
            <div class="title-group">
                <label data-aos="fade-right" data-aos-delay="100" class="title-tertiary"><?php echo $Banner['contact_us_label']; ?></label>
                <h2 data-aos="fade-right" data-aos-delay="200"><?php echo $Banner['contact_us_title']; ?></h2>
                <div class="description" data-aos="fade-right" data-aos-delay="300"><?php echo $Banner['contact_us_description']; ?></div>
            </div>

            <div class="my-4">
                <div class="d-flex align-items-center" data-aos="fade-up" data-aos-delay="350">
                    <div style="background-image:url('/wp-content/themes/i4/assets/images/icon-email.svg')" class="icon-sizes contain"></div>
                    <a href="mailto:<?php echo $Banner['contact_us_email']; ?>" class="t-tertiary ml-3"><?php echo $Banner['contact_us_email']; ?></a>
                </div>
                <div class="d-flex mt-3" data-aos="fade-up" data-aos-delay="400">
                    <div style="background-image:url('/wp-content/themes/i4/assets/images/icon-maps.svg')" class="icon-sizes contain"></div>
                    <div class="t-tertiary ml-3"><?php echo nl2br($Banner['address_details']); ?></div>
                </div>
                <div class="d-flex mt-3" data-aos="fade-up" data-aos-delay="450">
                    <div class="icon-phone"><i class="fas fa-phone-alt"></i></div>
                    <div class="t-tertiary ml-3"><?php echo nl2br($Banner['contact_us_numbers']); ?></div>
                </div>
            </div>

            <a target="_blank" href="<?php echo $Banner['contact_us_button']['link']; ?>" class="btn-paragraph"  data-aos="fade-right" data-aos-delay="400"><?php echo $Banner['contact_us_button']['label']; ?></a>
        </div>
        <div class="col-md-6 mt-md-0 mt-4">
            <div class="image-max-width">
                <div class="ratio-1-1 cover" data-aos="fade-up" data-aos-delay="500" style="background-image: url('<?php echo  $Banner['contact_us_image']; ?>')"></div>
            </div>
        </div>
    </div>
</div>



<div class="bg-dark py-section">
    <div class="content">
        <div class="contact-us-form">
            <div class="title-group">
                <h2 class="title">Contact Us</h2>
            </div>

            <?php $content = apply_filters('the_content', get_post_field('post_content', 39)); print_r($content); ?>
        </div>

    </div>

</div>


<?php /*

<div class="homepage-testimonials parallax-window" data-parallax="scroll" data-image-src="<?php echo $Banner['testimonials_background_image']; ?>" >
    <div class="content">
        <div class="title-group text-center content-smaller">
            <label data-aos="fade-up" data-aos-delay="0"><?php echo $Banner['testimonials_label']; ?></label>
            <h2 class="text-white" data-aos="fade-up" data-aos-delay="100"><?php echo $Banner['testimonials_tittle']; ?></h2>
        </div>
        <div class="subtitle text-white" data-aos="fade-up" data-aos-delay="200"><?php echo $Banner['testimonials_description']; ?></div>
        <sep></sep><sep></sep><sep></sep><sep></sep>
        <div class="carousel cards-carousel-full dark" style="min-height: 300px;" data-aos="fade-up" data-aos-delay="300">
            <?php foreach (get_posts([ "category_name" => "Testimonial"]) AS $testimonial){ $PostMeta = get_post_meta($testimonial->ID);  ?>
                <section class="carousel-item" href="javascript:;">
                    <div class="image" style="background-image: url('<?php echo wp_get_attachment_image_src($PostMeta['image'][0])[0]; ?>')"></div>
                    <div class="right">
                        <h3><?php echo $PostMeta['full_name'][0]; ?></h3>
                        <div class=""><?php echo $PostMeta['position'][0]; ?></div>
                    </div>
                    <div class="text"><?php echo $PostMeta['testimonial'][0]; ?></div>
                </section>
            <?php } ?>
        </div>
    </div>
</div>


<?php */ ?>

<?php /*

<div class="homepage-articles" data-aos="fade-up" data-aos-delay="0">
    <div class="content">
        <div class="title-group text-center content-smaller">
            <label data-aos="fade-up" data-aos-delay="100">Blog</label>
            <h2 class="" data-aos="fade-up" data-aos-delay="200">Daily Articles</h2>
        </div>
        <div class="blog-posts">
            <?php $i=0; foreach (get_posts([ "category_name" => "blog-media", "numberposts" => 3]) AS $posts){ $PostMeta = get_post_meta($posts->ID); $i++; ?>
                <?php
                $PostMeta = get_post_meta($posts->ID);
                ?>
                <a href="<?php echo $PostInfo->guid; ?>" class="waves-effect" data-aos="fade-up" data-aos-delay="<?php echo 200+$i*100; ?>">
                    <div class="image" style="background-image: url('<?php echo wp_get_attachment_image_src($PostMeta['image'][0])[0]; ?>')"></div>
                    <div class="date"><span class="material-icons">event</span> <?php echo $posts->post_date; ?></div>
                    <h2><?php echo $posts->post_title; ?></h2>
                    <div class="text text-gray"><?php echo substr(strip_tags($posts->post_content), 0,150); ?> ...</div>
                    <sep></sep>
                    <div class="learn_more left "><span>Learn More</span> <span class="material-icons arrow">arrow_forward</span></div>
                </a>
            <?php } ?>
        </div>
    </div>
</div>

*/ ?>

<?php get_footer(); ?>
