

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>
<body>
<?php $TopInfo = get_fields('56'); ?>
<?php $page = get_post_meta(get_the_ID()); ?>

<?php if(get_the_ID()!=56){ ?>
<header class="main_menu other_pages">

    <nav>
        <div class="content header-top-title">
            <?php if(array_key_exists("linkedin",$TopInfo) && array_key_exists("url",$TopInfo['linkedin']) && $TopInfo['linkedin']!=''){ ?><a href="<?php echo $TopInfo['linkedin']['url'] ?>" target="_blank"><?php echo htmlspecialchars_decode($TopInfo['linkedin']['title']); ?></a><?php } ?>

            <?php if(array_key_exists("header_top_title",$TopInfo) && $TopInfo['header_top_title']['title']!='' && $TopInfo['header_top_title']['url']){ ?>
                <a href="<?php echo $TopInfo['header_top_title']['url'] ?>" target="_blank"><?php echo $TopInfo['header_top_title']['title']; ?></a>
            <?php } ?>
        </div>
        <div class="content">
            <a href="#" class="toggle"><i class="fas fa-bars mobile-burgers"></i><i class="fas fa-times"></i></a>
            <a href="/" class="custom-logo-link" rel="home">
                <img width="228" height="53" src="/wp-content/themes/i4/assets/images/logo.svg" class="custom-logo" alt="i4">
            </a>
        <?php
        wp_nav_menu([
            "menu" => "primary",
            "theme_location" => "primary",
            "container" => "",
            "items_wrao" => "<ul></ul>",
        ]);




        wp_nav_menu([
            "menu" => "primary",
            "theme_location" => "primary",
            "container" => "",
            "items_wrao" => "<ul></ul>",
            "menu_class" => "mobile"
        ]);
        ?>
        </div>
    </nav>
</header>

<?php }else{ ?>
<header class="main_menu" id="main_menu">

    <nav>
        <div class="content header-top-title">
            <?php if(array_key_exists("linkedin",$TopInfo) && array_key_exists("url",$TopInfo['linkedin']) && $TopInfo['linkedin']!=''){ ?><a href="<?php echo $TopInfo['linkedin']['url'] ?>" target="_blank"><?php echo htmlspecialchars_decode($TopInfo['linkedin']['title']); ?></a><?php } ?>

            <?php if(array_key_exists("header_top_title",$TopInfo) && $TopInfo['header_top_title']['title']!='' && $TopInfo['header_top_title']['url']){ ?>
                <a href="<?php echo $TopInfo['header_top_title']['url'] ?>" target="_blank"><?php echo $TopInfo['header_top_title']['title']; ?></a>
            <?php } ?>
        </div>
        <div class="content">

            <a href="#" class="toggle"><i class="fas fa-bars mobile-burgers"></i><i class="fas fa-times"></i></a>
            <?php if(function_exists('the_custom_logo')){ the_custom_logo(); } ?>
            <?php
            wp_nav_menu([
                "menu" => "primary",
                "theme_location" => "primary",
                "container" => "",
                "items_wrao" => "<ul></ul>",
            ]);




            wp_nav_menu([
                "menu" => "primary",
                "theme_location" => "primary",
                "container" => "",
                "items_wrao" => "<ul></ul>",
                "menu_class" => "mobile"
            ]);
            ?>
        </div>
    </nav>
</header>
<?php  } ?>


<?php if(is_admin_bar_showing()){ ?>
<style>
    header.main_menu{ top:32px !important; }
    @media(max-width: 782px){
        header.main_menu{ top:46px !important; }
    }
</style>
<?php } ?>
