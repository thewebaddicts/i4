<div class="menu-spacer"></div>
<?php /* Template Name: Partners */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>

<?php $Banner = get_fields('374');  ?>


<div class="breadcrumbs">
    <div class="content">
        <a href="/" class="list-item py-2">Home</a>
        <label class="py-2"> / </label>
        <label class="py-2">Our Partners</label>
    </div>
</div>

<div class="content">


    <div class="py-section">
        <div class="title-group">
            <h2 class="title-primary text-center"  data-aos="fade-up" data-aos-delay="200"><?php echo $Banner['partners_title']; ?></h2>
            <div class="description text-center" style="max-width: 800px;width: 100%;margin-left: auto;margin-right: auto;"  data-aos="fade-up" data-aos-delay="300"><?php echo $Banner['partners_text']; ?></div>
        </div>

        <div class="row mt-3">
            <?php $i=0; foreach ($Banner['partners_list'] AS $partner){ $i++; ?>
                <div class="col-lg-2 col-md-3 col-4 mb-2">
                    <div class="partner-image ratio-3-2" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>" style="background-image: url('<?php echo  $partner['image']; ?>')"></div>
                </div>
            <?php } ?>
        </div>
    </div>



</div>




<?php get_footer(); ?>


