<?php /* Template Name: Contact Us */ ?>
<?php get_header(); ?>
<?php $Banner = get_fields('56'); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>
<!--<pre class="mt-100"> --><?php //print_r($meta); exit();?><!-- </pre>-->



<div class="breadcrumbs">
    <div class="content">
        <a href="/" class="list-item py-2">Home</a>
        <label class="py-2"> / </label>
        <label class="py-2">Contact us</label>
    </div>
</div>



<div class="content py-section">
    <div class="row align-items-center">
        <div class="col-md-6 pr-lg-5 pr-md-4">
            <div class="title-group">
                <label data-aos="fade-right" data-aos-delay="100" class="title-tertiary"><?php echo $Banner['contact_us_label']; ?></label>
                <h2 data-aos="fade-right" data-aos-delay="200"><?php echo $Banner['contact_us_title']; ?></h2>
                <div class="description" data-aos="fade-right" data-aos-delay="300"><?php echo $Banner['contact_us_description']; ?></div>
            </div>

            <div class="my-4">
                <div class="d-flex align-items-center" data-aos="fade-up" data-aos-delay="350">
                    <div style="background-image:url('/wp-content/themes/i4/assets/images/icon-email.svg')" class="icon-sizes contain"></div>
                    <a href="mailto:<?php echo $Banner['contact_us_email']; ?>" class="t-tertiary ml-3"><?php echo $Banner['contact_us_email']; ?></a>
                </div>
                <div class="d-flex mt-3" data-aos="fade-up" data-aos-delay="400">
                    <div style="background-image:url('/wp-content/themes/i4/assets/images/icon-maps.svg')" class="icon-sizes contain"></div>
                    <div class="t-tertiary ml-3"><?php echo nl2br($Banner['address_details']); ?></div>
                </div>
                <div class="d-flex mt-3" data-aos="fade-up" data-aos-delay="450">
                    <div class="icon-phone"><i class="fas fa-phone-alt"></i></div>
                    <div class="t-tertiary ml-3"><?php echo nl2br($Banner['contact_us_numbers']); ?></div>
                </div>
            </div>

            <a target="_blank" href="<?php echo $Banner['contact_us_button']['link']; ?>" class="btn-paragraph"  data-aos="fade-right" data-aos-delay="400"><?php echo $Banner['contact_us_button']['label']; ?></a>
        </div>
        <div class="col-md-6 mt-md-0 mt-4">
            <div class="image-max-width">
                <div class="ratio-1-1 cover" data-aos="fade-up" data-aos-delay="500" style="background-image: url('<?php echo  $Banner['contact_us_image']; ?>')"></div>
            </div>
        </div>
    </div>
</div>


<div class="bg-dark py-section">
    <div class="content">
        <div class="contact-us-form">
            <div class="title-group">
                <h2 class="title">Send us an Email</h2>
            </div>

            <?php the_content(); ?>
        </div>

    </div>

</div>






<?php get_footer(); ?>


